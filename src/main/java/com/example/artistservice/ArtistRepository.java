package com.example.artistservice;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import qt3.music.domains.Artist;

@Repository
public interface ArtistRepository extends ReactiveMongoRepository<Artist, String> {
}
