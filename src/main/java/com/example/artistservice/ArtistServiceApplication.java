package com.example.artistservice;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.function.client.WebClient;
import qt3.music.domains.Artist;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class ArtistServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArtistServiceApplication.class, args);
    }

    @Bean
    @LoadBalanced
    WebClient.Builder client() {return WebClient.builder();}

    @Bean
    ApplicationRunner applicationRunner(ArtistRepository artistRepository) {
        return args -> {
            var song = new Artist.Song(UUID.randomUUID().toString(), "title");
            var album = new Artist.Album(UUID.randomUUID().toString(), "name", List.of(song).toArray(Artist.Song[]::new));

            Artist artist1 = Artist.builder()
                    .id(UUID.randomUUID().toString())
                    .name("name")
                    .albums(List.of(album).toArray(Artist.Album[]::new))
                    .build();

            Artist artist2 = Artist.builder()
                    .id(UUID.randomUUID().toString())
                    .name("name")
                    .albums(List.of(album).toArray(Artist.Album[]::new))
                    .build();

            Stream.of(artist1, artist2).forEach(a -> artistRepository.save(a).subscribe());
//            artistRepository.findAll().subscribe(System.out::println);
        };
    }
}
