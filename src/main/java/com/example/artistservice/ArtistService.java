package com.example.artistservice;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import qt3.music.domains.Artist;
import qt3.music.domains.User;
import reactor.core.publisher.Flux;

@Service
@AllArgsConstructor
public class ArtistService {
    ArtistRepository artistRepository;

    WebClient.Builder webClient;

    public Flux<Artist> getAllArtists() {
        return artistRepository.findAll();
    }

    public Flux<User> getUsers() {
        return webClient.build().get()
                .uri("lb://user-service/all-users")
                .retrieve()
                .bodyToFlux(User.class);
    }
}
