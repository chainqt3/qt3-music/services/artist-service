package com.example.artistservice;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import qt3.music.domains.Artist;
import qt3.music.domains.User;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
@Slf4j
public class ArtistController {
    private final ArtistService artistService;

    @GetMapping("all-artists")
    public Flux<Artist> getAllArtists() {
        return artistService.getAllArtists();
    }

    @CircuitBreaker(name = "", fallbackMethod = "fallbackMethod")
    @GetMapping("all-users")
    public Flux<User> getUsersFromArtist() {
        log.info("all-users");
        return artistService.getUsers();
    }

    private Flux<User> fallbackMethod(Exception e) {
        log.error("fallbackMethod error: {}", String.valueOf(e.getCause()));
        User user = new User(UUID.randomUUID().toString(), "", "", "", "");
        List<User> users = List.of(user);
        return Flux.fromIterable(users);
    }
}
