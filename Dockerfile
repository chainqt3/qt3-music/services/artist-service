FROM openjdk:17-alpine

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

RUN apk update && apk add bash

COPY server-props/wait-for-it.sh /wait-for-it.sh
RUN chmod +x wait-for-it.sh

CMD ["./wait-for-it.sh", "config-server:9000", "--strict", "--timeout=300", "--", "java", "-Dspring.profiles.active=docker", "-jar", "/app.jar"]

